"""
This module is used to build a static website in pure HTML.
"""

import os
import re
import subprocess

from build.errors import raise_error
import settings


def is_pure_html(context):
    for folder in settings.GENERATOR_FOLDERS:
        new_path = os.path.join(context.source_path, folder)
        if os.path.exists(os.path.join(new_path, "index.html")):
            context.logger.info(f"Found something to build in '{new_path}'")
            context.source_path = new_path
            return True


class PureHTML:
    def __init__(self, context):
        self.name = "pure HTML"
        self.context = context

    def get_versions(self):
        pass

    def get_build_ready(self):
        pass

    def build_website(self):
        self.context.logger.info("Building website by just copying files")
        try:
            result = subprocess.run(
                [
                    "scripts/rsync.sh",
                    self.context.repository_path,
                    self.context.build_path,
                ],
                capture_output=True,
                timeout=settings.TIMEOUT_RSYNC,
            )
        except subprocess.TimeoutExpired:
            raise_error("timeout_build")

        self.context.logger.info("Website built")

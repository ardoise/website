from generators.hugo import Hugo, is_hugo
from generators.jekyll import Jekyll, is_jekyll
from generators.pelican import Pelican, is_pelican
from generators.pure_html import PureHTML, is_pure_html

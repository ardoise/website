# encoding: utf-8

from command import clean
from command import stats

commands = (clean.clean_websites, stats.stats_day, stats.stats_hour)

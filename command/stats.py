from datetime import date, datetime, timedelta
import json
import os

import click
from flask.cli import with_appcontext

from build.utils import get_size
import settings


def get_stat(hours):
    stat = {}

    # Compute storage space used by websites in MB
    size = get_size(settings.ARDOISE_PUBLISH) // 1000000
    stat["websites_size"] = size

    # Compute number of websites
    count = len(
        [
            name
            for name in os.listdir(settings.ARDOISE_PUBLISH)
            if os.path.isdir(os.path.join(settings.ARDOISE_PUBLISH, name))
        ]
    )
    stat["websites"] = count

    # Compute how much updates has been done during the last hours
    last_time = datetime.today() - timedelta(hours=hours)
    with open(settings.ARDOISE_SUBMISSIONS) as file_handler:
        submissions = json.load(file_handler)
    count = 0
    for repository in submissions:
        for submission in submissions[repository]:
            submission_date = datetime.strptime(
                submission["submission"], "%Y-%m-%d %H:%M:%S"
            )
            if submission_date >= last_time:
                count += 1
    stat["updates"] = count

    return stat


@click.command(help="Compute websites stats for last hour.")
@with_appcontext
def stats_hour():
    try:
        with open(settings.ARDOISE_STATS_HOURS) as file_handler:
            stats = json.load(file_handler)
    except FileNotFoundError:
        stats = []

    stat = get_stat(1)
    stat["hour"] = (datetime.today() - timedelta(hours=1)).strftime("%Hh")
    stats.append(stat)
    # Keep the last 24 hours
    stats = stats[-24:]
    with open(settings.ARDOISE_STATS_HOURS, "w") as file_handler:
        json.dump(stats, file_handler, indent=2)


@click.command(help="Compute websites stats for last day.")
@with_appcontext
def stats_day():
    try:
        with open(settings.ARDOISE_STATS_DAYS) as file_handler:
            stats = json.load(file_handler)
    except FileNotFoundError:
        stats = []

    stat = get_stat(24)
    stat["day"] = (datetime.today() - timedelta(days=1)).strftime("%Y-%m-%d")
    stats.append(stat)
    # Keep the last 30 days
    stats = stats[-30:]
    with open(settings.ARDOISE_STATS_DAYS, "w") as file_handler:
        json.dump(stats, file_handler, indent=2)

#!/bin/bash

cd $1
wget -q https://github.com/gohugoio/hugo/releases/download/v$2/hugo_extended_$2_linux-64bit.tar.gz
mkdir $1/hugo_$2
tar -xf hugo_extended_$2_linux-64bit.tar.gz -C $1/hugo_$2
rm hugo_extended_$2_linux-64bit.tar.gz
echo "Ok"

#!/bin/bash

cd $1
eval "$(~/.rbenv/bin/rbenv init - bash)"
bundle _$2_ exec jekyll _$3_ build -d $4
echo "Ok"

#!/bin/bash

rsync -avz --delete-after $1/ $2/
chmod g+rx $2/

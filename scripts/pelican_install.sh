#!/bin/bash

cd $1
mkdir $1/pelican_$2
cd $1/pelican_$2
python3 -m venv venv
venv/bin/pip install pelican==$2
echo "Ok"

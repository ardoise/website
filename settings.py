import logging
import os

from dotenv import load_dotenv


load_dotenv()

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
DEBUG = os.environ.get("FLASK_DEBUG", "False").lower() == "true"
SECRET_KEY = os.environ.get("FLASK_SECRET_KEY", "Choose a secret key")

BABEL_DEFAULT_LOCALE = os.environ.get("BABEL_DEFAULT_LOCALE", "en")

AVAILABLE_LANGUAGES = [
    ("fr", "Français"),
    ("en", "English"),
]

# Log File
LOGFILE = os.environ.get("LOGFILE", "/var/log/ardoise/ardoise.log")
LOGLEVEL = os.environ.get("LOGLEVEL", logging.DEBUG)
LOGFORMAT = os.environ.get("LOGFORMAT", "%(asctime)s.%(msecs)03d - %(message)s")
LOGDATE = os.environ.get("LOGDATE", "%Y-%m-%d %H:%M:%S")

# REDIS configuration
REDIS_URL = os.environ.get("REDIS_URL", "redis://")

# Ardoise version
ARDOISE_VERSION = os.environ.get("ARDOISE_VERSION", "0.1.1")

# Ardoise domain
ARDOISE_DOMAIN = os.environ.get("ARDOISE_DOMAIN", ".example.org")

# Ardoise subdomain forbidden list
ARDOISE_FORBIDDEN = os.environ.get(
    "ARDOISE_FORBIDDEN",
    ["www" + ARDOISE_DOMAIN, "admin" + ARDOISE_DOMAIN]
)

# Ardoise configuration
ARDOISE_SUBMISSIONS = os.environ.get("ARDOISE_SUBMISSIONS", "/var/ardoise/submissions.json")
ARDOISE_NAMES = os.environ.get("ARDOISE_NAMES", "/var/ardoise/names.json")
ARDOISE_PUBLISH = os.environ.get("ARDOISE_PUBLISH", "/var/ardoise/www")
ARDOISE_TOOLS = os.environ.get("ARDOISE_TOOLS", "/var/ardoise/tools")
ARDOISE_NGINX = os.environ.get("ARDOISE_NGINX", "/var/ardoise/nginx")
ARDOISE_CERTBOT = os.environ.get("ARDOISE_CERTBOT", "/var/ardoise/certs")
ARDOISE_LETSENCRYPT = os.environ.get("ARDOISE_LETSENCRYPT", "/var/ardoise/letsencrypt")
ARDOISE_STATS_HOURS = os.environ.get("ARDOISE_STATS_HOURS", "/var/ardoise/stats_hours.json")
ARDOISE_STATS_DAYS = os.environ.get("ARDOISE_STATS_DAYS", "/var/ardoise/stats_days.json")

# Days without update after which websites will be removed
DAYS_BEFORE_DELETION = int(os.environ.get("DAYS_BEFORE_DELETION", 90))

# Number of generations authorized on 24h for the same repository
MAX_GENERATIONS = int(os.environ.get("MAX_GENERATIONS", 5))

# Max size in bytes for a source repository
MAX_SIZE_REPOSITORY = int(os.environ.get("MAX_SIZE_REPOSITORY", 1000 * 1048576))

# Max size in bytes for generated static website
MAX_SIZE_WEBSITE = int(os.environ.get("MAX_SIZE_WEBSITE", 1000 * 1048576))

# Locale to use when computing folder sizes through "du"
LOCALE_DU = os.environ.get("LOCALE_DU", "fr_FR.UTF-8")

# folders to look for specific file(s) in order to gess which generator was used
GENERATOR_FOLDERS = os.environ.get("GENERATOR_FOLDERS", ("", "doc", "docs"))
if isinstance(GENERATOR_FOLDERS, str):
    GENERATOR_FOLDERS = GENERATOR_FOLDERS.split(",")

### Timeouts in seconds for each step

# Timeout for job
TIMEOUT_JOB = int(os.environ.get("TIMEOUT_JOB", 600))

# Timeouts for repositories
TIMEOUT_GUESS = int(os.environ.get("TIMEOUT_CLONE", 10))
TIMEOUT_CLONE = int(os.environ.get("TIMEOUT_CLONE", 240))
TIMEOUT_SIZE = int(os.environ.get("TIMEOUT_SIZE", 10))
TIMEOUT_RSYNC = int(os.environ.get("TIMEOUT_RSYNC", 60))
TIMEOUT_CERTIFICATE = int(os.environ.get("TIMEOUT_CERTIFICATE", 120))

# Timeouts for Ruby
TIMEOUT_RUBY_INIT = int(os.environ.get("TIMEOUT_RUBY_INIT", 10))
TIMEOUT_RUBY_CHECK = int(os.environ.get("TIMEOUT_RUBY_CHECK", 10))
TIMEOUT_RUBY_INSTALL = int(os.environ.get("TIMEOUT_RUBY_INSTALL", 480))

# Timeouts for Bundler
TIMEOUT_BUNDLER_CHECK = int(os.environ.get("TIMEOUT_BUNDLER_INSTALL", 10))
TIMEOUT_BUNDLER_INSTALL = int(os.environ.get("TIMEOUT_BUNDLER_INSTALL", 300))

# Timeouts for Jekyll
TIMEOUT_JEKYLL_INIT = int(os.environ.get("TIMEOUT_JEKYLL_BUILD", 60))
TIMEOUT_JEKYLL_BUILD = int(os.environ.get("TIMEOUT_JEKYLL_BUILD", 300))

# Jekyll settings
JEKYLL_GITHUB_RUBY_VERSION = os.environ.get("JEKYLL_DEFAULT_RUBY_VERSION", "2.7.3")
JEKYLL_GITHUB_BUNDLER_VERSION = os.environ.get("JEKYLL_DEFAULT_BUNDLER_VERSION", "2.4.10")
JEKYLL_GITHUB_JEKYLL_VERSION = os.environ.get("JEKYLL_DEFAULT_JEKYLL_VERSION", "3.9.3")
JEKYLL_DEFAULT_RUBY_VERSION = os.environ.get("JEKYLL_DEFAULT_RUBY_VERSION", "3.1.3")
JEKYLL_DEFAULT_BUNDLER_VERSION = os.environ.get("JEKYLL_DEFAULT_BUNDLER_VERSION", "2.4.12")
JEKYLL_DEFAULT_JEKYLL_VERSION = os.environ.get("JEKYLL_DEFAULT_JEKYLL_VERSION", "4.3.2")
JEKYLL_MINIMAL_BUNDLER_VERSION = os.environ.get("JEKYLL_MINIMAL_BUNDLER_VERSION", "2.2.6")

# Hugo settings
HUGO_DEFAULT_VERSION = os.environ.get("HUGO_DEFAULT_VERSION", "0.118.2")
TIMEOUT_HUGO_INSTALL = int(os.environ.get("TIMEOUT_HUGO_INSTALL", 300))
TIMEOUT_HUGO_POSTCSS_INSTALL = int(os.environ.get("TIMEOUT_HUGO_POSTCSS_INSTALL", 300))
TIMEOUT_HUGO_BUILD = int(os.environ.get("TIMEOUT_HUGO_BUILD", 300))

# Pelican settings
PELICAN_DEFAULT_VERSION = os.environ.get("PELICAN_DEFAULT_VERSION", "4.9.1")
PELICAN_DEFAULT_PYTHON_VERSION = os.environ.get("PELICAN_DEFAULT_PYTHON_VERSION", "3.11")
TIMEOUT_PELICAN_INSTALL = int(os.environ.get("TIMEOUT_PELICAN_INSTALL", 300))
TIMEOUT_PELICAN_BUILD = int(os.environ.get("TIMEOUT_PELICAN_BUILD", 300))
PELICAN_DEFAULT_PYTHON_VERSION
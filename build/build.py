"""
This module is used to build a static website and to publish it.
"""

from datetime import datetime, timedelta
import json
import logging
import os
import re
import shutil
import subprocess
import tempfile

from jinja2 import Environment, FileSystemLoader
import redis
import rq

from build.errors import ArdoiseError, raise_error
from build.utils import get_size
from generators import *
import settings


class Logger:
    def __init__(self, name, identifier):
        logging.basicConfig(
            filename=settings.LOGFILE,
            encoding="utf-8",
            level=settings.LOGLEVEL,
            format=settings.LOGFORMAT,
            datefmt=settings.LOGDATE,
        )
        self.logger = logging.getLogger(__name__)
        self.identifier = identifier
        self.name = name

    def info(self, message):
        self.logger.info(f"INFO - {self.name} - {self.identifier} - {message}")

    def error(self, message):
        self.logger.error(f"ERROR - {self.name} - {self.identifier} - {message}")

    def debug(self, message):
        self.logger.debug(f"DEBUG - {self.name} - {self.identifier} - {message}")


class Job:
    def __init__(self, job):
        self.job = job
        self.job.meta["logs"] = []
        self.job.meta["stop"] = False

    def send(self, message):
        self.job.meta["logs"].append(message)
        self.job.save_meta()

    def stop(self):
        self.job.meta["stop"] = True
        self.job.save_meta()


class Builder:
    def __init__(self, repository, identifier):
        self.identifier = identifier
        self.logger = Logger(repository, identifier)
        self.job = Job(rq.get_current_job())
        self.job = Job(rq.get_current_job())
        if repository[-1] == "/":
            self.repository = repository[:-1]
        else:
            self.repository = repository
        self.name = ""
        self.domain = []
        self.repository_path = ""
        self.source_path = ""
        self.target_name = ""
        self.build_path = ""
        self.configuration_path = ""
        self.target_path = ""
        self.generator = None
        self.version_control_system = ""

    def build(self):
        self.logger.info(f"Building website from {self.repository}")
        self.job.send(f"Génération du site web à partir du dépôt {self.repository}")

        # Creating a temporary structure of directories for building website
        main_folder = tempfile.TemporaryDirectory()
        repository_folder = tempfile.TemporaryDirectory(dir=main_folder.name)
        build_folder = tempfile.TemporaryDirectory(dir=main_folder.name)
        configuration_folder = tempfile.TemporaryDirectory(dir=main_folder.name)
        self.repository_path = repository_folder.name
        self.source_path = repository_folder.name
        self.build_path = build_folder.name
        self.configuration_path = configuration_folder.name

        try:
            self.submit()
            self.get_repository()
            self.analyze_repository()
            self.get_build_ready()
            self.build_website()
            self.set_configuration()
            self.publish_website()
            self.logger.info("Publishing finished")
            self.job.send("Publication terminée.")
            result = "success"
        except ArdoiseError as exception:
            result = exception.message[:6]
            self.logger.error(exception.log)
            if exception.stacktrace is not None:
                self.logger.error(exception.stacktrace)
            self.job.send(exception.message)
        # ~ except Exception as exception:
            # ~ result = "ERR999"
            # ~ self.logger.error("ERR999: Unknown error")
            # ~ self.logger.error(exception)
            # ~ self.job.send("ERR999: Une erreur inconnue est survenue.")

        last_update = None
        try:
            with open(settings.ARDOISE_SUBMISSIONS) as file_handler:
                submissions = json.load(file_handler)
        except FileNotFoundError:
            submissions = {}
        if self.repository in submissions:
            for submission in submissions[self.repository]:
                if submission["identifier"] == self.identifier:
                    submission["result"] = result
                    last_update = submission["submission"]
            with open(settings.ARDOISE_SUBMISSIONS, "w") as file_handler:
                json.dump(submissions, file_handler, indent=2)

        if result == "success":
            try:
                with open(settings.ARDOISE_NAMES) as file_handler:
                    names = json.load(file_handler)
            except FileNotFoundError:
                names = {}
            if self.repository not in names:
                names[self.repository] = {
                    "name": self.name,
                    "last_update": None,
                }
            names[self.repository]["last_update"] = last_update
            with open(settings.ARDOISE_NAMES, "w") as file_handler:
                json.dump(names, file_handler, indent=2)
            if len(self.domain) == 1:
                self.job.send(
                    "Le site est désormais accessible à l'adresse "
                    f"<a href='https://{self.domain[0]}'>https://{self.domain[0]}</a>"
                )
            else:
                response = "Le site est désormais accessible aux adresses "
                for domain in self.domain:
                    response += f"<a href='https://{domain}'>https://{domain}</a> "
                self.job.send(response)

        self.job.stop()

    def submit(self):
        if not self.repository.startswith("http"):
            raise_error("bad_url")

        try:
            with open(settings.ARDOISE_SUBMISSIONS) as file_handler:
                submissions = json.load(file_handler)
        except FileNotFoundError:
            submissions = {}
        if self.repository not in submissions:
            submissions[self.repository] = []

        generations = [
            submission["submission"] for submission in submissions[self.repository]
        ]
        if len(generations) >= settings.MAX_GENERATIONS:
            oldest_generation = datetime.strptime(min(generations), "%Y-%m-%d %H:%M:%S")
            yesterday = datetime.today() - timedelta(hours=24)
            if oldest_generation > yesterday:
                raise_error("limit_per_24h", settings.MAX_GENERATIONS)

        limit = settings.MAX_GENERATIONS - 1
        submissions[self.repository] = submissions[self.repository][-limit:] + [
            {
                "submission": datetime.today().strftime("%Y-%m-%d %H:%M:%S"),
                "identifier": self.identifier,
                "result": "",
            }
        ]
        with open(settings.ARDOISE_SUBMISSIONS, "w") as file_handler:
            json.dump(submissions, file_handler, indent=2)

    def get_repository(self):
        self.version_control_system = self.guess_version_control_system()
        if self.version_control_system == "git":
            self.logger.info("Cloning repository")
            self.job.send("Récupération du dépôt.")
            self.clone_repository_with_git()
            return
        elif self.version_control_system == "mercurial":
            self.logger.info("Cloning repository")
            self.job.send("Récupération du dépôt.")
            self.clone_repository_with_mercurial()
            return
        elif self.version_control_system is not None:
            raise_error("vcs_not_supported", self.version_control_system)
        else:
            raise_error("unknown_vcs")

    def guess_version_control_system(self):
        try:
            result = subprocess.run(
                ["git", "ls-remote", self.repository],
                capture_output=True,
                timeout=settings.TIMEOUT_GUESS,
            )
        except subprocess.TimeoutExpired:
            raise_error("timeout_git")

        if result.returncode == 0:
            self.logger.info(f"Version control system found: git")
            return "git"

        # Is it mercurial ?
        try:
            result = subprocess.run(
                ["hg", "identify", self.repository],
                capture_output=True,
                timeout=settings.TIMEOUT_GUESS,
            )
        except subprocess.TimeoutExpired:
            raise_error("timeout_mercurial")

        if result.returncode == 0:
            self.logger.info(f"Version control system found: mercurial")
            return "mercurial"

    def clone_repository_with_git(self):
        shutil.rmtree(self.source_path, ignore_errors=True)
        try:
            result = subprocess.run(
                [
                    "git",
                    "clone",
                    "--depth",
                    "1",
                    "--single-branch",
                    "--quiet",
                    "--no-tags",
                    self.repository,
                    self.source_path,
                ],
                capture_output=True,
                timeout=settings.TIMEOUT_CLONE,
            )
        except subprocess.TimeoutExpired:
            raise_error("timeout_clone")

        # Manage unknown clone error
        if not os.path.exists(self.source_path):
            raise_error("error_clone")

        self.logger.info("Repository cloned")

    def clone_repository_with_mercurial(self):
        shutil.rmtree(self.source_path, ignore_errors=True)
        try:
            result = subprocess.run(
                [
                    "hg",
                    "clone",
                    "--noninteractive",
                    "--quiet",
                    self.repository,
                    self.source_path,
                ],
                capture_output=True,
                timeout=settings.TIMEOUT_CLONE,
            )
        except subprocess.TimeoutExpired:
            raise_error("timeout_clone")

        # Manage unknown clone error
        if not os.path.exists(self.source_path):
            raise_error("error_clone")

        self.logger.info("Repository cloned")

    def analyze_repository(self):
        """
        Analyze the repository in order to be able to build it or not.
        """
        self.job.send("Analyse du dépôt.")
        # Compute project size
        size = get_size(self.source_path)
        if size == -1:
            raise_error("error_size")
        self.logger.info(f"Repository size: {size}")
        if size > settings.MAX_SIZE_REPOSITORY:
            raise_error("repository_size")

        # Retrieve generator
        self.generator = self.retrieve_generator()
        self.logger.info(f"Static site generator found: {self.generator.name}")
        self.generator.get_versions()

        # Retrieve a name for this website
        self.select_name()
        self.domain = [f"{self.name}{settings.ARDOISE_DOMAIN}"]
        # From RFC-952, a name is a text string up to 24 characters drawn from the
        # alphabet (A-Z), digits (0-9), minus sign (-), and period (.).
        self.domain = [self.domain[0].replace("_", "-")]

        # Is there a CNAME file for a specific domain name ?
        if os.path.exists(os.path.join(self.source_path, "CNAME")):
            with open(os.path.join(self.source_path, "CNAME")) as file_handle:
                self.domain.append(
                    file_handle.read().replace("_", "-").replace("\n", "")
                )

        # Is name authorized ?
        for name in settings.ARDOISE_FORBIDDEN:
            for domain in self.domain:
                if domain == name:
                    raise_error("forbidden_domain", domain)

    def retrieve_generator(self):
        if is_jekyll(self):
            return Jekyll(self)
        elif is_hugo(self):
            return Hugo(self)
        elif is_pelican(self):
            return Pelican(self)
        elif is_pure_html(self):
            return PureHTML(self)
        elif self.generator is not None:
            raise_error("generator_not_supported", self.generator.name)
        else:
            raise_error("generator_unknown")

    def select_name(self):
        """
        Selecting project's name, this name will be the URL unique identifier.
        """
        try:
            with open(settings.ARDOISE_NAMES) as file_handler:
                names = json.load(file_handler)
        except FileNotFoundError:
            names = {}

        if self.repository not in names:
            self.name = self.repository.split("/")[-1]
            if self.name.endswith(".git"):
                self.name = self.name[:-4]
            pattern = re.compile(f"^{self.name}(_[0-9]+)?$")
            times = 0
            for name in [name["name"] for name in names.values()]:
                if pattern.match(name):
                    times += 1
            if times > 0:
                self.name = f"{self.name}_{times}"
            names[self.repository] = {
                "name": self.name,
                "last_update": None,
            }
            self.logger.info(f"New unique name selected: {self.name}")
        else:
            self.name = names[self.repository]["name"]
            self.logger.info(f"Already known name picked up: {self.name}")
        self.target_name = self.name.replace(".", "_")
        self.target_path = os.path.join(settings.ARDOISE_PUBLISH, self.target_name)

    def get_build_ready(self):
        """
        With retrieved information, install needed tools.
        """
        self.job.send("Préparation de la génération.")
        self.generator.get_build_ready()

    def build_website(self):
        self.job.send("Génération du site.")
        self.generator.build_website()

    def set_configuration(self):
        self.logger.info("Configuring domain")
        # Generating nginx configuration file
        environment = Environment(
            loader=FileSystemLoader("templates"),
            trim_blocks=True,
        )
        context = {
            "site": self.target_name,
            "domain": " ".join(self.domain),
        }
        result = environment.get_template("configuration/nginx.conf").render(context)
        config_path = os.path.join(self.configuration_path, f"{self.target_name}.conf")
        with open(config_path, "w") as file_handle:
            file_handle.write(result)
        if os.path.getsize(config_path) == 0:
            raise_error("error_nginx")

        # Generating a new certificate if necessary
        if not settings.DEBUG:
            self.logger.info("Generating certificate")
            try:
                result = subprocess.run(
                    [
                        "scripts/certbot_create.sh",
                        # Simple domain for now, for certificates
                        # " -d ".join(self.domain),
                        self.domain[0],
                        self.domain[0],
                        self.configuration_path,
                        settings.ARDOISE_LETSENCRYPT,
                    ],
                    capture_output=True,
                    timeout=settings.TIMEOUT_CERTIFICATE,
                )
            except subprocess.TimeoutExpired:
                raise_error("timeout_certificate")

            config_path = os.path.join(self.configuration_path, f"{self.domain[0]}.pem")
            cert_path = os.path.join(settings.ARDOISE_CERTBOT, f"{self.domain[0]}.pem")
            if os.path.getsize(config_path) == 0:
                raise_error("error_certificate")

    def publish_website(self):
        self.job.send("Publication du site.")
        # Verify website final size
        size = get_size(self.build_path)
        if size == -1:
            raise_error("error_size")
        self.logger.info(f"Website size: {size}")
        if size > settings.MAX_SIZE_REPOSITORY:
            raise_error("result_size")

        # Copy necessary files to final destination through rsync
        self.logger.info(f"Publishing website files")
        try:
            result = subprocess.run(
                [
                    "scripts/rsync.sh",
                    self.build_path,
                    self.target_path,
                ],
                capture_output=True,
                timeout=settings.TIMEOUT_RSYNC,
            )
        except subprocess.TimeoutExpired:
            raise_error("timeout_copy")

        # Copying nginx config
        config_path = os.path.join(self.configuration_path, f"{self.target_name}.conf")
        nginx_path = os.path.join(settings.ARDOISE_NGINX, f"{self.target_name}.conf")
        shutil.copyfile(config_path, nginx_path)

        # Copying certificate
        if not settings.DEBUG:
            config_path = os.path.join(self.configuration_path, f"{self.domain[0]}.pem")
            cert_path = os.path.join(settings.ARDOISE_CERTBOT, f"{self.domain[0]}.pem")
            shutil.copyfile(config_path, cert_path)


def build(repository, identifier):
    Builder(repository, identifier).build()

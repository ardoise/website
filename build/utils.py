import locale
import subprocess

import settings


def get_size(path):
    locale.setlocale(locale.LC_ALL, settings.LOCALE_DU)
    units = {
        "kb": 1000,
        "mb": 1000000,
        "gb": 1000000000,
        "tb": 1000000000000,
    }
    result = subprocess.run(["du", "-s", "-BKB", path], capture_output=True)
    size = result.stdout.decode("utf-8").split()[0]
    for unit, factor in units.items():
        if unit in size.lower():
            return int(locale.atof(size[:-2]) * factor)

    return -1

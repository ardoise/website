[uwsgi]
module = wsgi:app

master = true
processes = 5

socket = /var/ardoise/ardoise.sock
chmod-socket = 664
uid = ardoise
gid = www-data
vacuum = true

die-on-term = true

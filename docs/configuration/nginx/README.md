Il y a 3 fichiers principaux.

## nginx.conf

Il s'agit d'ajouter à la configuration de base de nginx un lien vers le répertoire des
configurations nginx pour les sites générés.

## certbot.conf

Il s'agit de la configuration nginx pour donner accès à certbot à des fichiers sur le
serveur pour générer les certificats SSL des sites générés.

## ardoise.conf

Il s'agit de la configuration nginx pour le site principal d'ardoise.

Il y a 2 fichiers.

## ardoise.service

Il s'agit du fichier permettant d'intégrer le site ardoise à systemd. Ce fichier est
donc à copier dans /etc/systemd/system/ puis de rafraichir systemd :

    systemctl daemon-reload

Puis de lancer l'application :

    systemctl start ardoise.service

## ardoise-rq.service

Il s'agit du fichier permettant d'intégrer le gestionnaire de queue à systemd. Ce
fichier est donc à copier dans /etc/systemd/system/ puis de rafraichir systemd :

    systemctl daemon-reload

Puis de lancer la gestion de la queue :

    systemctl start ardoise-rq.service

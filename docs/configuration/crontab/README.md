Chacun des fichiers doit être lancé via cron.

## stats-hour

Ce fichier permet de calculer les statistiques du site de manière horaire.

## stats-day

Ce fichier permet de calculer les statistiques du site de manière journalière.

## clean-websites

Ce fichier permet de supprimer les sites non renouvelés depuis trop longtemps.

